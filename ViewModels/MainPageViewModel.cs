﻿using Microsoft.Win32;
using System.Windows.Input;
using System.Windows.Navigation;
using WeatherApp.Views;

namespace WeatherApp.ViewModels
{
    public class MainPageViewModel:BaseViewModel
    {
        private ICommand _navigateSettingsCommand;
        private ICommand _navigateWeatherCommand;
        private ICommand _pickImageCommand;
        private string _imageSource;
        private bool _canExecute;  
        private NavigationService _navigation;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="MainPageViewModel"/> class.
        /// </summary>
        public MainPageViewModel(NavigationService navigation)
        {
            _navigation = navigation;
            _canExecute = true;
            ImageSource = @"C:\img.png";
        }

        /// <summary>
        /// Gets or sets the image source.
        /// </summary>
        /// <value>
        /// The image source.
        /// </value>
        public string ImageSource
        {
            get => _imageSource;
            set
            {
                _imageSource = value;
                RaisePropertyChanged(nameof(ImageSource));
            }
        }

        /// <summary>
        /// Gets the navigate weather command.
        /// </summary>
        /// <value>
        /// The navigate weather command.
        /// </value>
        public ICommand NavigateWeatherCommand => _navigateWeatherCommand ?? (_navigateWeatherCommand = new CommandHandler(() => NavigateWeather(), _canExecute));

        /// <summary>
        /// Gets the navigate settings command.
        /// </summary>
        /// <value>
        /// The navigate settings command.
        /// </value>
        public ICommand NavigateSettingsCommand => _navigateSettingsCommand ?? (_navigateSettingsCommand = new CommandHandler(() => NavigateSettings(), _canExecute));


        /// <summary>
        /// Gets the pick image command.
        /// </summary>
        /// <value>
        /// The pick image command.
        /// </value>
        public ICommand PickImageCommand
        {
            get
            {
                return _pickImageCommand ?? (_pickImageCommand = new CommandHandler(() => OpenFileDialog(), _canExecute));
            }
        }

        private void NavigateWeather()
        {
            App.MainFrame.NavigationService.Navigate(new WeatherPage());
        }

        private void NavigateSettings()
        {
            App.MainFrame.NavigationService.Navigate(new SettingsPage());
        }

        /// <summary>
        /// Opens the file dialog.
        /// </summary>
        public void OpenFileDialog()
        {
            _canExecute = false;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image files (*.jpg, *.png)|*.jpg; *.png";
            if (openFileDialog.ShowDialog() == true)
            {
                ImageSource = openFileDialog.FileName;
            }
            _canExecute = true;
        }
    }
}
